swift.vim
=========

The files in this repository are copied from the [swift][0] repository. The
commit used was  `33371c1 (Merge pull request #843 from
practicalswift/typo-fixes-20160101, 2016-01-01)`. The rational for copying them
is to ease the use with [Vundle][1].



[0]: https://github.com/apple/swift
[1]: https://github.com/VundleVim/Vundle.vim.git
